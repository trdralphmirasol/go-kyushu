<?php get_header(); ?>

<!-- 全体warapper -->
<div class="wrapper">


<!-- メインwrap -->
<div id="main">


<!-- コンテンツブロック -->
<div class="row">

<div class="latest-midashi"><?php echo (get_option('midashi'));?></div>

<!-- 本文エリア -->
<ul class="block-three">

<!-- 最新記事列 -->
<?php if (have_posts()) : ?>

<!-- 投稿ループ -->
<?php while (have_posts()) : the_post(); ?>

<!-- アイテム -->
<li class="item">
<div class="item-img"><a href="<?php the_permalink(); ?>"><?php
if ( has_post_thumbnail() ) the_post_thumbnail(array(300,200));
else echo '<img src="'.get_template_directory_uri().'/images/noimage-630x420.jpg" />';
?></a></div>
<div class="item-date"><?php echo get_post_time('M j, Y'); ?></div>
<div class="item-cat"><?php the_category(' / '); ?></div>
<h2 class="item-title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php echo mb_substr($post->post_title, 0, 27); ?></a></h2>
<p class="item-text"><?php echo mb_substr(get_the_excerpt(), 0, 69); ?><a href="<?php the_permalink(); ?>">...</a></p>
<p class="bt-dl"><a href="<?php the_permalink(); ?>">続きを読む</a></p>
</li>
<!-- / アイテム -->

<?php endwhile; ?>
<!-- / 投稿ループ -->

</ul>
<!-- / 本文エリア -->

<!-- 投稿がない場合 -->
<?php else: ?> 
<p style="text-align:center">　　No Post.</p>
<?php endif; ?>
<!-- / 投稿がない場合 -->



</div>
<!-- / コンテンツブロック -->


</div>
<!-- / メインwrap -->


</div>
<!-- / 全体wrapper -->


<!-- Latestブロック -->
<div id="latest-bloc">

<!-- コンテンツブロック -->
<div class="row">
<article class="latest">	
<div id="topbox">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('トップページ下部') ) : ?>
<?php endif; ?>
</div>
</article>	
</div>
<!-- / コンテンツブロック -->
</div>
<!-- / Latestブロック -->


<?php get_footer(); ?>