<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title><?php echo trim(wp_title('', false)); if(wp_title('', false)) { echo ' - '; } bloginfo('name'); ?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/responsive.css" type="text/css" media="screen, print" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, print" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php
wp_deregister_script('jquery');
wp_enqueue_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js', array(), '1.7.1');
?>
<?php wp_head(); ?>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/jquery/scrolltopcontrol.js"></script>
<script src="<?php bloginfo('template_directory');?>/jquery/jquery.cycle2.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory');?>/jquery/jquery.cycle2.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
 $().ready(function() {
   $(document).ready(
     function(){
     $("a img").hover(function(){
     $(this).fadeTo(200, 0.8);
     },function(){
     $(this).fadeTo(300, 1.0);
     });
   });
 });
$(function(){
    var box    = $("#nav");
    var boxTop = box.offset().top;
    $(window).scroll(function () {
        if($(window).scrollTop() >= boxTop) {
            box.addClass("fixed");
			$("body").css("margin-top","40px");
        } else {
            box.removeClass("fixed");
			$("body").css("margin-top","0px");
        }
    });
});
</script>

</head>

<body <?php body_class(); ?>>

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '242031986160771',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


<!-- スライドショー -->
<div id="top-slide">
<div class="cycle-slideshow"
 data-cycle-fx="fade"
 data-cycle-pause-on-hover="false"
 data-cycle-speed="300">
<img src="<?php echo (get_option('slideshow1')) ? get_option('slideshow1') : get_template_directory_uri() . '/images/main_01.jpg' ?>" alt="<?php bloginfo('name'); ?>" />
<img src="<?php echo (get_option('slideshow2')) ? get_option('slideshow2') : get_template_directory_uri() . '/images/main_02.jpg' ?>" alt="<?php bloginfo('name'); ?>" />
<img src="<?php echo (get_option('slideshow3')) ? get_option('slideshow3') : get_template_directory_uri() . '/images/main_03.jpg' ?>" alt="<?php bloginfo('name'); ?>" />
</div>
</div>
<!-- / スライドショー -->

<!-- トップナビゲーション -->
<nav id="nav" class="main-navigation" role="navigation">
<?php wp_nav_menu( array( 'menu' => 'topnav', 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
</nav>
<!-- / トップナビゲーション -->
<div class="clear"></div>  

<!-- ヘッダー -->
<header id="header">

<!-- ヘッダー中身 -->    
<div class="header-inner">

<!-- ロゴ -->
<h1 class="logo">
<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo (get_option('logo_url')) ? get_option('logo_url') : get_template_directory_uri() .'/images/logo.gif' ?>" alt="<?php bloginfo('name'); ?>"/></a>
<div class="blog-name"><?php bloginfo('name'); ?></div>
</h1>
<!-- / ロゴ -->
  
</div>    
<!-- / ヘッダー中身 -->    

</header>
<!-- / ヘッダー -->  
<div class="clear"></div>