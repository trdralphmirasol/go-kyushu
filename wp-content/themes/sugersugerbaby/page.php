<?php get_header('single'); ?>


<!-- 全体warapper -->
<div class="wrapper">

<!-- メインwrap -->
<div id="main">

<h2 class="pagetitle2"><?php the_title(); ?></h2>

<!-- コンテンツブロック -->
<div class="row">


<!-- 本文エリア -->
<article class="twothird">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php the_content(); ?>

<?php endwhile; else: ?>
<p><?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
<?php endif; ?>

</article>
<!-- / 本文エリア -->


<!-- サイドエリア -->
<article class="third">

<?php get_sidebar(); ?>

</article>
<!-- / サイドエリア -->


</div>
<!-- / コンテンツブロック -->


</div>
<!-- / メインwrap -->

</div>
<!-- / 全体wrapper -->

<?php get_footer(); ?>