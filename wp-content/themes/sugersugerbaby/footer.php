
<!-- フッターエリア -->
<footer id="footer">
<div class="footer-inner">

<div id="sociallink">
<!-- Twitterアイコン -->
<a href="<?php echo (get_option('twitter'));?>" target="_blank" title="Twitter"><img src="<?php bloginfo('template_directory');?>/images/ic_foot_twitter.gif" alt="Twitter" style="margin-right:10px" /></a>
<!-- / Twitterアイコン -->
<!-- Facebookアイコン -->
<a href="<?php echo (get_option('facebook'));?>" target="_blank" title="Facebook"><img src="<?php bloginfo('template_directory');?>/images/ic_foot_facebook.gif" alt="Facebook" style="margin-right:10px" /></a>
<!-- / Facebookアイコン -->
<!-- RSSアイコン -->
<a href="<?php bloginfo('rss2_url'); ?>" target="_blank" title="RSS Feed"><img src="<?php bloginfo('template_directory');?>/images/ic_foot_rss.gif" alt="RSS Feed" /></a>
<!-- / RSSアイコン -->
</div>


<!-- コピーライト表示 -->
<div id="copyright">
© <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>. all rights reserved.
</div>
<!-- /コピーライト表示 -->

</div>
</footer>
<!-- / フッターエリア -->

<?php wp_footer(); ?>


</body>
</html>