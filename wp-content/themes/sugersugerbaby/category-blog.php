<?php get_header('single'); ?>


<!-- 全体warapper -->
<div class="wrapper">

<!-- メインwrap -->
<div id="main">

<!-- 投稿が存在するかを確認する条件文 -->
<?php if (have_posts()) : ?>

<!-- 投稿一覧の最初を取得 -->
<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

<!-- カテゴリーアーカイブの場合 -->
<?php /* If this is a category archive */ if (is_category()) { ?>
<div class="pagetitle"><?php single_cat_title(); ?></div>

<!-- タグアーカイブの場合 -->
<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
<div class="pagetitle"><?php single_tag_title(); ?></div>

<!-- 日別アーカイブの場合 -->
<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
<div class="pagetitle"><?php printf(_c(' %s|Daily archive page', 'kubrick'), get_the_time(__('F jS, Y'))); ?>年</div>

<!-- 月別アーカイブの場合 -->
<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
<div class="pagetitle"><?php printf(_c(' %s|Monthly archive page', 'kubrick'), get_the_time(__('Y-m'))); ?></div>

<!-- 年別アーカイブの場合 -->
<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
<div class="pagetitle"><?php printf(_c('Archive for %s|Yearly archive page', 'kubrick'), get_the_time(__('Y'))); ?></div>

<!-- 著者アーカイブの場合 -->
<?php /* If this is an author archive */ } elseif (is_author()) { ?>
<div class="pagetitle"><?php _e('Author Archive'); ?></div>

<!-- 複数ページアーカイブの場合 -->
<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
<div class="pagetitle"><?php _e('Blog Archives'); ?></div>

<?php } ?>
<!-- / 投稿一覧の最初 -->


<!-- コンテンツブロック -->
<div class="row">

<!-- 本文エリア -->
<article class="twothird">

<!-- 投稿ループ -->
<?php while (have_posts()) : the_post(); ?>

<div class="blog-date"><?php echo get_post_time('M j, Y'); ?></div>
<h2 class="blog-title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

<?php the_content('<p class="bt-dl">続きを読む</p>') ?>

<div id="blog-foot"><?php printf(__('Posted in %s'), get_the_category_list(', ')); ?> ｜ <?php comments_popup_link(__('No Comments &#187;'), __('1 Comment &#187;'), __('% Comments &#187;'), '', __('Comments Closed') ); ?>　<?php edit_post_link(__('Edit'), ''); ?></div>
<p style="margin-bottom:50px">　</p>

<?php endwhile; ?>
<!-- / 投稿ループ -->

<!-- 投稿がない場合 -->
<?php else :
        if ( is_category() ) { // If this is a category archive
            printf("<h4>".__("　　投稿がありません").'</h4>', single_cat_title('',false));
        } else if ( is_date() ) { // If this is a date archive
            echo('<h4>'.__("Sorry, but there aren't any posts with this date.").'</h4>');
        } else if ( is_author() ) { // If this is a category archive
            $userdata = get_userdatabylogin(get_query_var('author_name'));
            printf("<h4>".__("Sorry, but there aren't any posts by %s yet.")."</h4>", $userdata->display_name);
        } else {
            echo("<h4>".__('No posts found.').'</h4>');
        }
    endif;
?>
<!-- / 投稿がない場合 -->



<!-- ページャー -->
<div class="pager">
<?php global $wp_rewrite;
$paginate_base = get_pagenum_link(1);
if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
	$paginate_format = '';
	$paginate_base = add_query_arg('paged','%#%');
}
else{
	$paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
	user_trailingslashit('page/%#%/','paged');;
	$paginate_base .= '%_%';
}
echo paginate_links(array(
	'base' => $paginate_base,
	'format' => $paginate_format,
	'total' => $wp_query->max_num_pages,
	'mid_size' => 4,
	'current' => ($paged ? $paged : 1),
	'prev_text' => '«',
	'next_text' => '»',
)); ?>
</div>
<!-- / ページャー -->

</article>
<!-- / 本文エリア -->


<!-- サイドエリア -->
<article class="third">

<?php get_sidebar(); ?>

</article>
<!-- / サイドエリア -->


</div>
<!-- / コンテンツブロック -->


</div>
<!-- / メインwrap -->

</div>
<!-- / 全体wrapper -->

<?php get_footer(); ?>