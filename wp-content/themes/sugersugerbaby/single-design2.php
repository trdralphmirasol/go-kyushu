<?php
/*
Template Name Posts: サイドバー無し
*/
?>

<?php get_header('single'); ?>

<!-- 全体warapper -->
<div class="wrapper">

<!-- メインwrap -->
<div id="main">

<!-- ページタイトル -->
<h2 class="pagetitle"><?php the_category(' / '); ?></h2>
<!-- / ページタイトル -->

<!-- コンテンツブロック -->
<div class="row">

<!-- 本文エリア -->
<article>

<!-- 投稿 -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="blog-date2"><?php echo get_post_time('M j, Y'); ?></div>
<h1 class="blog-title2"><?php the_title(); ?></h1>

<?php the_content(); ?>


<!-- ウィジェットエリア（投稿記事下） -->
<div class="row widget-adspace">
<article>	
<div id="topbox">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('投稿記事下') ) : ?>
<?php endif; ?>
</div>
</article>	
</div>
<!-- / ウィジェットエリア（投稿記事下） -->


<div id="blog-foot"><?php printf(__('Posted in %s'), get_the_category_list(', ')); ?> ｜ <?php comments_popup_link(__('No Comments &#187;'), __('1 Comment &#187;'), __('% Comments &#187;'), '', __('Comments Closed') ); ?>　<?php edit_post_link(__('Edit'), ''); ?></div>
<!-- / 投稿 -->


<!-- 関連記事 -->
<h3 class="similar-head">関連記事</h3>
<div class="similar">
<?php
foreach((get_the_category()) as $cat) {
$catid = $cat->cat_ID ;
break ;
}
$thisID = $post->ID;
$get_posts_parm = "'numberposts=3&category=" . $catid . "&exclude=". $thisID . "'";
?>
<ul>
<?php $posts = get_posts($get_posts_parm); ?>
<?php foreach($posts as $post): ?>
<li><table class="similar-text"><tr><th><a href="<?php the_permalink(); ?>"><?php
if ( has_post_thumbnail() ) the_post_thumbnail();
else echo '<img src="'.get_template_directory_uri().'/images/noimage.jpg" />';
?></a></th>
<td><h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"> <?php the_title(); ?></a></h4></td></tr></table></li>
<?php endforeach;
wp_reset_postdata(); ?>
</ul>
</div>
<!-- / 関連記事 -->


<!-- ページャー -->
<div id="next">
<span class="left"><?php previous_post_link('%link', '＜ %title', 'true'); ?></span>
<span class="right"><?php next_post_link('%link', '%title ＞', 'true'); ?></span>
<div class="clear"></div>
</div>
<!-- / ページャー -->

<!-- コメントエリア -->
<?php comments_template(); ?>
<!-- / コメントエリア -->

<!-- 投稿が無い場合 -->
<?php endwhile; else: ?>
<p><?php echo "お探しの記事、ページは見つかりませんでした。"; ?></p>
<?php endif; ?>
<!-- 投稿が無い場合 -->


<!-- ウィジェットエリア（関連記事下） -->
<div class="row widget-adspace">
<article>	
<div id="topbox">
<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('関連記事下') ) : ?>
<?php endif; ?>
</div>
</article>	
</div>
<!-- / ウィジェットエリア（関連記事下） -->


</article>
<!-- / 本文エリア -->




</div>
<!-- / コンテンツブロック -->


</div>
<!-- / メインwrap -->

</div>
<!-- / 全体wrapper -->

<?php get_footer(); ?>